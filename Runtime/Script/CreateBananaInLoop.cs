﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateBananaInLoop : MonoBehaviour
{
    public GameObject m_prefab;
    public float m_delayBetweenSpawn = 2f;
    public float m_range=2;
    public Transform m_parent;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnSomeRandomBanana", 0, m_delayBetweenSpawn);
    }

 
    // Update is called once per frame
    public void SpawnSomeRandomBanana()
    {
      GameObject obj = Instantiate(m_prefab, transform.position + (new Vector3(UnityEngine.Random.value, UnityEngine.Random.value, UnityEngine.Random.value) * m_range), Quaternion.identity);
        obj.transform.parent = m_parent;
    }
}
