# How to use: 2019_08_15_BananaSpawner   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.bananaspawner":"https://gitlab.com/eloistree/2019_08_15_BananaSpawner.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.bananaspawner",                              
  "displayName": "Banana Spawner",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "A simple banana spawner script and models",                         
  "keywords": ["Script","Tool","Spawner","Banana"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    