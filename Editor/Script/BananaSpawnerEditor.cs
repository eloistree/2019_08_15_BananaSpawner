﻿
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CreateBananaInLoop))]
public class BananaSpawnerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CreateBananaInLoop myScript = (CreateBananaInLoop)target;
        if (GUILayout.Button("More Banana"))
        {
            myScript.SpawnSomeRandomBanana();
        }
    }
}